package com.example.examen

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        restartGame(this)
    }
    var Player1 = ArrayList<Int>()
    var Player2 = ArrayList<Int>()
    var ActivePlayer = 1


    fun restartGame(view: MainActivity)
    {
        bt1.setBackgroundColor(Color.MAGENTA)
        bt2.setBackgroundColor(Color.MAGENTA)
        bt3.setBackgroundColor(Color.MAGENTA)
        bt4.setBackgroundColor(Color.MAGENTA)
        bt5.setBackgroundColor(Color.MAGENTA)
        bt6.setBackgroundColor(Color.MAGENTA)
        bt7.setBackgroundColor(Color.MAGENTA)
        bt8.setBackgroundColor(Color.MAGENTA)
        bt9.setBackgroundColor(Color.MAGENTA)

        bt1.text = ""
        bt2.text = ""
        bt3.text = ""
        bt4.text = ""
        bt5.text = ""
        bt6.text = ""
        bt7.text = ""
        bt8.text = ""
        bt9.text = ""

        Player1.clear()
        Player2.clear()
        ActivePlayer = 1

        bt1.isEnabled = true
        bt2.isEnabled = true
        bt3.isEnabled = true
        bt4.isEnabled = true
        bt5.isEnabled = true
        bt6.isEnabled = true
        bt7.isEnabled = true
        bt8.isEnabled = true
        bt9.isEnabled = true


    }

    fun buttonClick(view: View)
    {
        val buSelected: Button = view as Button
        var cellId = 0
        when(buSelected.id)
        {
            R.id.bt1 -> cellId = 1
            R.id.bt2 -> cellId = 2
            R.id.bt3 -> cellId = 3

            R.id.bt4 -> cellId = 4
            R.id.bt5 -> cellId = 5
            R.id.bt6 -> cellId = 6

            R.id.bt7 -> cellId = 7
            R.id.bt8 -> cellId = 8
            R.id.bt9 -> cellId = 9
        }
        PlayGame(cellId,buSelected)

    }




    fun PlayGame(cellId:Int,buSelected:Button)
    {
        if (ActivePlayer == 1)
        {
            buSelected.text = "+"
            buSelected.setBackgroundColor(Color.RED)
            Player1.add(cellId)
            ActivePlayer = 2

                try {
                    AutoPlay()
                }catch (ex:Exception)
                {

                }

        }
        else
        {
            buSelected.text = "+"
            buSelected.setBackgroundColor(Color.GREEN)
            Player2.add(cellId)
            ActivePlayer = 1
        }
        buSelected.isEnabled = false
        CheckWinner()
    }

    fun CheckWinner()
    {
        var winner = -1


        if (Player1.contains(1) && Player1.contains(2) && Player1.contains(3))
        {
            winner = 1
        }
        if (Player2.contains(1) && Player2.contains(2) && Player2.contains(3))
        {
            winner = 2
        }


        if (Player1.contains(4) && Player1.contains(5) && Player1.contains(6))
        {
            winner = 1
        }
        if (Player2.contains(4) && Player2.contains(5) && Player2.contains(6))
        {
            winner = 2
        }


        if (Player1.contains(7) && Player1.contains(8) && Player1.contains(9))
        {
            winner = 1
        }
        if (Player2.contains(7) && Player2.contains(8) && Player2.contains(9))
        {
            winner = 2
        }


        if (Player1.contains(1) && Player1.contains(4) && Player1.contains(7))
        {
            winner = 1
        }
        if (Player2.contains(1) && Player2.contains(4) && Player2.contains(7))
        {
            winner = 2
        }


        if (Player1.contains(2) && Player1.contains(5) && Player1.contains(8))
        {
            winner = 1
        }
        if (Player2.contains(2) && Player2.contains(5) && Player2.contains(8))
        {
            winner = 2
        }


        if (Player1.contains(3) && Player1.contains(6) && Player1.contains(9))
        {
            winner = 1
        }
        if (Player2.contains(3) && Player2.contains(6) && Player2.contains(9))
        {
            winner = 2
        }


        if (Player1.contains(1) && Player1.contains(5) && Player1.contains(9))
        {
            winner = 1
        }
        if (Player2.contains(1) && Player2.contains(5) && Player2.contains(9))
        {
            winner = 2
        }


        if (Player1.contains(3) && Player1.contains(5) && Player1.contains(7))
        {
            winner = 1
        }
        if (Player2.contains(3) && Player2.contains(5) && Player2.contains(7))
        {
            winner = 2
        }

        if (winner != -1)
        {
            if (winner == 1) {
                stopTouch()
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Ganador!!")
                builder.setMessage("Gracias por participar Roberto")
                builder.setPositiveButton("Reiniciar") { _,_ -> restartGame(this) }
                builder.show()
            }
            else
            {
                stopTouch()
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Ganador!!")
                builder.setMessage("Gracias por participar iMac")
                builder.setPositiveButton("Reiniciar") { _,_ -> restartGame(this) }
                builder.show()
            }
        }
    }

    fun stopTouch()
    {
        bt1.isEnabled = false
        bt2.isEnabled = false
        bt3.isEnabled = false
        bt4.isEnabled = false
        bt5.isEnabled = false
        bt6.isEnabled = false
        bt7.isEnabled = false
        bt8.isEnabled = false
        bt9.isEnabled = false
    }

    fun AutoPlay()
    {
        val emptyCells = ArrayList<Int>()
        for (cellId in 1..9) {
            if (Player1.contains(cellId) || Player2.contains(cellId))
            {}
            else
            {
                emptyCells.add(cellId)
            }
        }

        val r = Random()
        val randomIndex = r.nextInt(emptyCells.size-0)+0
        val cellId = emptyCells[randomIndex]

        val buSelect:Button?
        when(cellId)
        {
            1 -> buSelect = bt1
            2 -> buSelect = bt2
            3 -> buSelect = bt3
            4 -> buSelect = bt4
            5 -> buSelect = bt5
            6 -> buSelect = bt6
            7 -> buSelect = bt7
            8 -> buSelect = bt8
            9 -> buSelect = bt9
            else -> buSelect = bt1
        }

        PlayGame(cellId,buSelect)
    }

}
